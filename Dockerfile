
# start from an official image
FROM python:3.6-alpine
# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apk update \
    # psycopg2 dependencies
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev
# arbitrary location choice: you can change the directory
RUN mkdir -p /usr/src/services
COPY . /usr/src/services/
WORKDIR /usr/src/services
# install our dependencies
# we use --system flag because we don't need an extra virtualenv
RUN pip install pipenv && pipenv install --system
# expose the port 8000
EXPOSE 8000
# define the default command to run when starting the container
CMD ["gunicorn", "--chdir", "app", "--bind", ":8000", "app.wsgi:application"] 